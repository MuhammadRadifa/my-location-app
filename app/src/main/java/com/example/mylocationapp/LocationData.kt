package com.example.mylocationapp

data class LocationData(
    val latitude:Double,
    val longitude:Double
)
